package main

import (
    "log"
    "net/http"
    "os"
)

var targetBaseURL string

func redirect(w http.ResponseWriter, r *http.Request) {
    url := targetBaseURL + r.RequestURI
    
    log.Printf("redirecting to %s", url)
    http.Redirect(w, r, url, http.StatusMovedPermanently)
}


func main() {
    targetBaseURL = os.Getenv("TARGET_BASE_URL")
    
    http.HandleFunc("/", redirect)
    err := http.ListenAndServe(":8080", nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}
