# README #

A simple redirector made in GoLang, optimized to run as Docker container.

### USAGE ###
Setup env vars:
`TARGET_BASE_URL=<https://service.com/>`


### BUILDING ###

1. Build statically linked app
`CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .`

2. Update certificates
`curl -o ca-certificates.crt https://raw.githubusercontent.com/bagder/ca-bundle/master/ca-bundle.crt`

3. Build docker image
`docker build -t http-redirect -f Dockerfile.scratch .`